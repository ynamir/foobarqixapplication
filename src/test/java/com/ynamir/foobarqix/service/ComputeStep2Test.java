package com.ynamir.foobarqix.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ynamir.foobarqix.exception.InputException;
import com.ynamir.foobarqix.serviceImpl.ComputeStep2;
import com.ynamir.foobarqix.serviceImpl.ModuloServiceImpl;

public class ComputeStep2Test {

	private Compute computeStep2;
	private ModuloService moduloServiceImpl;
	
	@Before
	public void initTest() {
		moduloServiceImpl = Mockito.mock(ModuloServiceImpl.class);
		computeStep2 = new ComputeStep2(moduloServiceImpl);
	}

	@Test
	public void computeStep20() {
		when(moduloServiceImpl.witchModulo(0)).thenReturn("FooBarQix");
		assertEquals("FooBarQix*", computeStep2.compute("0"));
	}

	@Test
	public void computeStep21() {
		when(moduloServiceImpl.witchModulo(1)).thenReturn("");
		assertEquals("1", computeStep2.compute("1"));
	}
	
	@Test
	public void computeStep2FooFoo() {
		when(moduloServiceImpl.witchModulo(3)).thenReturn("Foo");
		assertEquals("FooFoo", computeStep2.compute("3"));
	}
	
	@Test
	public void computeStep2BarBar() {
		when(moduloServiceImpl.witchModulo(5)).thenReturn("Bar");
		assertEquals("BarBar", computeStep2.compute("5"));
	}
	
	@Test
	public void computeStep2QixQix() {
		when(moduloServiceImpl.witchModulo(7)).thenReturn("Qix");
		assertEquals("QixQix", computeStep2.compute("7"));
	}
	
	@Test
	public void computeStep2Foo() {
		when(moduloServiceImpl.witchModulo(6)).thenReturn("Foo");
		assertEquals("Foo", computeStep2.compute("6"));
	}

	@Test
	public void computeStep2Bar() {
		when(moduloServiceImpl.witchModulo(10)).thenReturn("Bar");
		assertEquals("Bar*", computeStep2.compute("10"));
	}

	@Test
	public void computeStep2Qix() {
		when(moduloServiceImpl.witchModulo(14)).thenReturn("Qix");
		assertEquals("Qix", computeStep2.compute("14"));
	}

	@Test
	public void computeStep2FooBarBar() {
		when(moduloServiceImpl.witchModulo(15)).thenReturn("FooBar");
		assertEquals("FooBarBar", computeStep2.compute("15"));
	}

	@Test
	public void computeStep2FooQix() {
		when(moduloServiceImpl.witchModulo(21)).thenReturn("FooQix");
		assertEquals("FooQix", computeStep2.compute("21"));
	}

	@Test
	public void computeStep2BarQix() {
		when(moduloServiceImpl.witchModulo(35)).thenReturn("BarQix");
		assertEquals("BarQixFooBar", computeStep2.compute("35"));
	}

	@Test
	public void computeStep2FooBarQix() {
		when(moduloServiceImpl.witchModulo(105)).thenReturn("FooBarQix");
		assertEquals("FooBarQix*Bar", computeStep2.compute("105"));
	}
	
	@Test
	public void computeStep253() {
		when(moduloServiceImpl.witchModulo(53)).thenReturn("");
		assertEquals("BarFoo", computeStep2.compute("53"));
	}
	
	@Test
	public void computeStep251() {
		when(moduloServiceImpl.witchModulo(51)).thenReturn("Foo");
		assertEquals("FooBar", computeStep2.compute("51"));
	}
	
	@Test
	public void computeStep233() {
		when(moduloServiceImpl.witchModulo(33)).thenReturn("Foo");
		assertEquals("FooFooFoo", computeStep2.compute("33"));
	}
	
	@Test
	public void computeStep221() {
		when(moduloServiceImpl.witchModulo(21)).thenReturn("FooQix");
		assertEquals("FooQix", computeStep2.compute("21"));
	}
	
	@Test
	public void computeStep213() {
		when(moduloServiceImpl.witchModulo(13)).thenReturn("");
		assertEquals("Foo", computeStep2.compute("13"));
	}
	
	@Test
	public void computeStep2101() {
		when(moduloServiceImpl.witchModulo(101)).thenReturn("");
		assertEquals("1*1", computeStep2.compute("101"));
	}
	
	@Test
	public void computeStep2303() {
		when(moduloServiceImpl.witchModulo(303)).thenReturn("Foo");
		assertEquals("FooFoo*Foo", computeStep2.compute("303"));
	}
	
	@Test
	public void computeStep210101() {
		when(moduloServiceImpl.witchModulo(10101)).thenReturn("FooQix");
		assertEquals("FooQix**", computeStep2.compute("10101"));
	}

	@Test (expected =  InputException.class)
	public void computeStep2InputExcpetion() {
		computeStep2.compute("aaaa");
	}

	@Test (expected =  InputException.class)
	public void computeStep2Null() {
		computeStep2.compute(null);
	}

}

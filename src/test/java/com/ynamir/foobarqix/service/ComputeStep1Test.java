package com.ynamir.foobarqix.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ynamir.foobarqix.exception.InputException;
import com.ynamir.foobarqix.serviceImpl.ComputeStep1;
import com.ynamir.foobarqix.serviceImpl.ModuloServiceImpl;

public class ComputeStep1Test {

	private Compute computeStep1;
	private ModuloService moduloServiceImpl;
	
	@Before
	public void initTest() {
		moduloServiceImpl = Mockito.mock(ModuloServiceImpl.class);
		computeStep1 = new ComputeStep1(moduloServiceImpl);
	}

	@Test
	public void computeStep10() {
		when(moduloServiceImpl.witchModulo(0)).thenReturn("FooBarQix");
		assertEquals("FooBarQix", computeStep1.compute("0"));
	}

	@Test
	public void computeStep11() {
		when(moduloServiceImpl.witchModulo(1)).thenReturn("");
		assertEquals("1", computeStep1.compute("1"));
	}
	
	@Test
	public void computeStep1FooFoo() {
		when(moduloServiceImpl.witchModulo(3)).thenReturn("Foo");
		assertEquals("FooFoo", computeStep1.compute("3"));
	}
	
	@Test
	public void computeStep1BarBar() {
		when(moduloServiceImpl.witchModulo(5)).thenReturn("Bar");
		assertEquals("BarBar", computeStep1.compute("5"));
	}
	
	@Test
	public void computeStep1QixQix() {
		when(moduloServiceImpl.witchModulo(7)).thenReturn("Qix");
		assertEquals("QixQix", computeStep1.compute("7"));
	}
	
	@Test
	public void computeStep1Foo() {
		when(moduloServiceImpl.witchModulo(6)).thenReturn("Foo");
		assertEquals("Foo", computeStep1.compute("6"));
	}

	@Test
	public void computeStep1Bar() {
		when(moduloServiceImpl.witchModulo(10)).thenReturn("Bar");
		assertEquals("Bar", computeStep1.compute("10"));
	}

	@Test
	public void computeStep1Qix() {
		when(moduloServiceImpl.witchModulo(14)).thenReturn("Qix");
		assertEquals("Qix", computeStep1.compute("14"));
	}

	@Test
	public void computeStep1FooBarBar() {
		when(moduloServiceImpl.witchModulo(15)).thenReturn("FooBar");
		assertEquals("FooBarBar", computeStep1.compute("15"));
	}

	@Test
	public void computeStep1FooQix() {
		when(moduloServiceImpl.witchModulo(21)).thenReturn("FooQix");
		assertEquals("FooQix", computeStep1.compute("21"));
	}

	@Test
	public void computeStep1BarQix() {
		when(moduloServiceImpl.witchModulo(35)).thenReturn("BarQix");
		assertEquals("BarQixFooBar", computeStep1.compute("35"));
	}

	@Test
	public void computeStep1FooBarQix() {
		when(moduloServiceImpl.witchModulo(105)).thenReturn("FooBarQix");
		assertEquals("FooBarQixBar", computeStep1.compute("105"));
	}
	
	@Test
	public void computeStep153() {
		when(moduloServiceImpl.witchModulo(53)).thenReturn("");
		assertEquals("BarFoo", computeStep1.compute("53"));
	}
	
	@Test
	public void computeStep151() {
		when(moduloServiceImpl.witchModulo(51)).thenReturn("Foo");
		assertEquals("FooBar", computeStep1.compute("51"));
	}
	
	@Test
	public void computeStep133() {
		when(moduloServiceImpl.witchModulo(33)).thenReturn("Foo");
		assertEquals("FooFooFoo", computeStep1.compute("33"));
	}
	
	@Test
	public void computeStep121() {
		when(moduloServiceImpl.witchModulo(21)).thenReturn("FooQix");
		assertEquals("FooQix", computeStep1.compute("21"));
	}
	
	@Test
	public void computeStep113() {
		when(moduloServiceImpl.witchModulo(13)).thenReturn("");
		assertEquals("Foo", computeStep1.compute("13"));
	}
	
	@Test
	public void computeStep1101() {
		when(moduloServiceImpl.witchModulo(101)).thenReturn("");
		assertEquals("101", computeStep1.compute("101"));
	}
	
	@Test
	public void computeStep1303() {
		when(moduloServiceImpl.witchModulo(303)).thenReturn("Foo");
		assertEquals("FooFooFoo", computeStep1.compute("303"));
	}
	
	@Test
	public void computeStep110101() {
		when(moduloServiceImpl.witchModulo(10101)).thenReturn("FooQix");
		assertEquals("FooQix", computeStep1.compute("10101"));
	}

	@Test (expected =  InputException.class)
	public void computeStep1InputExcpetion() {
		computeStep1.compute("aaaa");
	}

	@Test (expected =  InputException.class)
	public void computeStep1Null() {
		computeStep1.compute(null);
	}

}

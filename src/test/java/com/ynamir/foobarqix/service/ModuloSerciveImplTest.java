package com.ynamir.foobarqix.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import com.ynamir.foobarqix.serviceImpl.ModuloServiceImpl;

public class ModuloSerciveImplTest {

	private ModuloService moduloService;
	
	@Before
	public void initTest() {
		moduloService = new ModuloServiceImpl();
	}

	@Test
	public void computeStep10() {
		assertEquals("FooBarQix", moduloService.witchModulo(0));
	}

	@Test
	public void computeStep11() {
		assertEquals("", moduloService.witchModulo(1));
	}
	
	@Test
	public void computeStep13() {
		assertEquals("Foo", moduloService.witchModulo(3));
	}
	
	@Test
	public void computeStep15() {
		assertEquals("Bar", moduloService.witchModulo(5));
	}
	
	@Test
	public void computeStep17() {
		assertEquals("Qix", moduloService.witchModulo(7));
	}

	@Test
	public void computeStep1Foo() {
		assertEquals("Foo", moduloService.witchModulo(6));
	}

	@Test
	public void computeStep1Bar() {
		assertEquals("Bar", moduloService.witchModulo(10));
	}

	@Test
	public void computeStep1Qix() {
		assertEquals("Qix", moduloService.witchModulo(14));
	}

	@Test
	public void computeStep1FooBar() {
		assertEquals("FooBar", moduloService.witchModulo(15));
	}

	@Test
	public void computeStep1FooQix() {
		assertEquals("FooQix", moduloService.witchModulo(21));
	}

	@Test
	public void computeStep1BarQix() {
		assertEquals("BarQix", moduloService.witchModulo(35));
	}

	@Test
	public void computeStep1FooBarQix() {
		assertEquals("FooBarQix", moduloService.witchModulo(105));
	}
	
	@Test
	public void computeStep153() {
		assertEquals("", moduloService.witchModulo(53));
	}
	
	@Test
	public void computeStep151() {
		assertEquals("Foo", moduloService.witchModulo(51));
	}
	
	@Test
	public void computeStep133() {
		assertEquals("Foo", moduloService.witchModulo(33));
	}
	
	@Test
	public void computeStep121() {
		assertEquals("FooQix", moduloService.witchModulo(21));
	}
	
	@Test
	public void computeStep113() {
		assertEquals("", moduloService.witchModulo(13));
	}
	
}

package com.ynamir.foobarqix;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ynamir.foobarqix.serviceImpl.ComputeStep1;
import com.ynamir.foobarqix.serviceImpl.ComputeStep2;

public class FoobarqixApplicationTests {

	private FoobarqixApplication obj;
	private ComputeStep1 cmpS1;
	private ComputeStep2 cmpS2;
	
	@Before
	public void initTest() {
		cmpS1 = Mockito.mock(ComputeStep1.class);
		cmpS2 = Mockito.mock(ComputeStep2.class);
		obj = new FoobarqixApplication(cmpS1, cmpS2);
	}

	@SuppressWarnings("static-access")
	@Test(expected = RuntimeException.class)
	public void mainTestFailed() {
		String[] args = {""};
		obj.main(args);
	}
	
	@SuppressWarnings("static-access")
	public void mainTest() {
		String[] args = {""};
		when(cmpS1.compute(anyString())).thenReturn(anyString());
		obj.main(args);
		verify(cmpS1).compute(anyString());
	}

}

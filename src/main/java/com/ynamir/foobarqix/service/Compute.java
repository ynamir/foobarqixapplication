package com.ynamir.foobarqix.service;

public interface Compute {

	public String compute(String number);
	
}

package com.ynamir.foobarqix.exception;

public class InputException extends RuntimeException {

	private static final long serialVersionUID = -5352100808675306287L;

	@SuppressWarnings("unused")
	private Exception ex;

	@SuppressWarnings("unused")
	private String msg;
	
	public InputException(String _msg, Exception _ex) {
		this.msg = _msg;
		this.ex = _ex;
	}
	
}

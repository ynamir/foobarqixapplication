package com.ynamir.foobarqix.serviceImpl;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.ynamir.foobarqix.service.ModuloService;

@Service
public class ModuloServiceImpl implements ModuloService{

	Map<Integer, String> modulo;
	
	public ModuloServiceImpl(Map<Integer, String> modulo) {
		this.modulo = modulo;
	}
	
	public ModuloServiceImpl() {
		this.modulo = new HashMap<Integer, String>();
		modulo.put(3, "Foo");
		modulo.put(5, "Bar");
		modulo.put(7, "Qix");
	}

	public String witchModulo(int digit) {
		
		StringBuilder sb = new StringBuilder();
		this.modulo.forEach((key, value) -> {
			if (digit%key == 0) {
				sb.append(value);
			}
		});
		return sb.toString();
	}
	
}

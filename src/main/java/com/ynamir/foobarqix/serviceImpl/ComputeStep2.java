package com.ynamir.foobarqix.serviceImpl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynamir.foobarqix.exception.InputException;
import com.ynamir.foobarqix.service.Compute;
import com.ynamir.foobarqix.service.ModuloService;

@Service
public class ComputeStep2 implements Compute {
	
    private static Logger logger = Logger.getLogger(ComputeStep2.class);
    
    private ModuloService moduloService;
    
    @Autowired
	public ComputeStep2(ModuloService moduloService) {
    	this.moduloService = moduloService;
	}

	public String compute(String number) {
		String result = "";
		int num;

		try {
			num = Integer.parseInt(number);
		}catch (NumberFormatException e) {
			logger.error(e.getMessage());
			throw new InputException("This string cannot be parsed", e);
		}

		String matche = number.replaceAll("3", "Foo").replaceAll("5", "Bar").replaceAll("7", "Qix");
		result += this.moduloService.witchModulo(num);
		result = result.isEmpty() && matche.replaceAll("\\d", "").isEmpty() ? number.replaceAll("0", "*") : result + matche.replaceAll("0", "*").replaceAll("\\d", "") ;
		
		return result;
	}

}

package com.ynamir.foobarqix;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ynamir.foobarqix.service.Compute;
import com.ynamir.foobarqix.serviceImpl.ComputeStep1;
import com.ynamir.foobarqix.serviceImpl.ComputeStep2;
import com.ynamir.foobarqix.serviceImpl.ModuloServiceImpl;



public class FoobarqixApplication {

    private static Logger logger = Logger.getLogger(FoobarqixApplication.class);


	private Compute computeStep1;
	private Compute computeStep2;

	@Autowired
	public FoobarqixApplication(ComputeStep1 computeStep1, ComputeStep2 computeStep2) {
		this.computeStep1 = computeStep1;
		this.computeStep2 = computeStep2;
	}
	
	public static void main(String[] args) {

		if(args.length == 0) {
			logger.info("usage : foobarqix string");
		} else {
			for (String string : args) {
				if (string != null) {
					
					FoobarqixApplication foobarqixApplication = new FoobarqixApplication(
							new ComputeStep1(new ModuloServiceImpl()), 
							new ComputeStep2(new ModuloServiceImpl()));
					logger.info("ComptueStep1(" + string + ") => " + foobarqixApplication.computeStep1.compute(string));
					logger.info("ComptueStep2(" + string + ") => " + foobarqixApplication.computeStep2.compute(string));
				}
			}
		}
	}
}

# FooBarQixApplication

* Development :
    - The application FooBarQix implemants two functions computeStep1 and computeStep2 respecting the following rules :
        - computeStep1 :
            - take a string as an input
            - return a string as an output
            - if the number is divisible by 3, write “Foo” instead of the number
            - if the number is divisible by 5, add “Bar”
            - if the number is divisible by 7, add “Qix”
            - for each digit 3, 5, 7, add “Foo”, “Bar”, “Qix” in the digits order.
        - computeStep2 :
            - take a string as an input
            - return a string as an output
            - if the number is divisible by 3, write “Foo” instead of the number
            - if the number is divisible by 5, add “Bar”
            - if the number is divisible by 7, add “Qix”
            - for each digit 0, 3, 5, 7, add "*", “Foo”, “Bar”, “Qix” in the digits order.

    - The two functions consumes the same service that takes as input a number and returns a string if the number is divisible by 3, 5 and 7.

    - I relied on the tests to implement both functions, so at first I created the tests based on the project statement and then i implemented both functions.

* Testing :
    - To verify tests we launch the following maven command : mvn test

* Packaging :
    - To generate the application jar we run the following maven command : mvn package -Dmaven.test.skip=true

* Running :
    - to run the FooBarQixApplication we have to launch the following command on the generated jar : 
        - giving one input : java -jar foobarqix-0.0.1-SNAPSHOT.jar 105
        - giving multiple inputs : java -jar foobarqix-0.0.1-SNAPSHOT.jar 3 5 7 15 21 35 105